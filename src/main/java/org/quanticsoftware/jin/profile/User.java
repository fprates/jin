package org.quanticsoftware.jin.profile;

import org.quanticsoftware.automata.core.Compose;

public class User {

	public long id;
	public String username, url;
	
	@Override
	public final String toString() {
		return (url == null)? Long.toString(id) : Compose.execute(id, "@", url);
	}
}
