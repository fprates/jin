package org.quanticsoftware.jin.profile;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ProfileContext {
	
	public User user;
	public List<Object[]> warnings;
	public Map<String, Object> data;
	
	public ProfileContext() {
		user = new User();
		warnings = new LinkedList<>();
	}
	
	public final void addFollower(String user) {
		get("followers").add(user);
	}
	
	public final void addFollowing(String user) {
		get("following").add(user);
	}
	
	public final boolean checkAuthority(String autorization) {
		return get("accesses").contains(autorization);
	}
	
	@SuppressWarnings("unchecked")
	private final Set<String> get(String type) {
		return (Set<String>)data.get(type);
	}
	
	public final Set<String> getFollowers() {
		return get("followers");
	}
	
	public final Set<String> getFollowing() {
		return get("following");
	}
	
	public final boolean isFollower(String user) {
		return get("followers").contains(user);
	}
	
	public final boolean isFollowing(String user) {
		return get("following").contains(user);
	}
	
	public final void removeFollower(String user) {
		get("followers").remove(user);
	}
	
	public final void removeFollowing(String user) {
		get("following").remove(user);
	}
}
