package org.quanticsoftware.jin.profile;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.quanticsoftware.automata.core.Compose;
import org.quanticsoftware.automata.core.Context;
import org.quanticsoftware.automata.core.Conversion;
import org.quanticsoftware.automata.core.DataObject;
import org.quanticsoftware.automata.core.Send;
import org.quanticsoftware.automata.facilities.Session;
import org.quanticsoftware.automata.servlet.pages.DataFormTool;
import org.quanticsoftware.automata.servlet.pages.PageDefinitionContext;
import org.quanticsoftware.automata.servlet.pages.PageDefinition;
import org.quanticsoftware.automata.servlet.pages.PageElement;
import org.quanticsoftware.jin.Shared;
import org.quanticsoftware.jin.frontend.FrontendContext;

public class ProfilePage implements PageDefinition {
	public static final int FOLLOWER_ADD = 0;
	public static final int FOLLOWER_REMOVE = 1;
	private String visible;
	
	@Override
	public final void execute(PageDefinitionContext pagedef) {
		pagedef.elements.get("head").title = "profile";
		
		var container = Shared.execute(pagedef);
		
		visible = pagedef.pagectx.template.css.get("input-button");
		
		container.button("home").targets.put("click", "home.page_redirect");
		container.button("follow").renderer = (s,e)->followrender(s,e);
		container.button("unfollow").renderer = (s,e)->unfollowrender(s,e);
		
		var form = DataFormTool.instance(container, pagedef.pagectx.type);
		
		var config = form.config("username");
		config.nolabel = true;
		config.einstance = (e,n)->e.custom("h1", n);
		
		config = form.config("url");
		config.nolabel = true;
		config.einstance = (e,n)->e.custom("h5", n);
		
		config = form.config("message");
		config.nolabel = true;
		config.einstance = (e,n)->e.p(n);
		
		config = form.config("history");
		config.nolabel = true;
		config.einstance = (e,n)->e.div(n);
		
		form.config("search").nolabel = true;
		
		form.build();
		
		form.get("search_group").
			button("search").
			targets.put("click", "profile_search");
		
		form.get("profile_page.history").
			renderer = (s,e)->{
				FrontendContext sessionctx = (FrontendContext)s.sessionctx();
				Shared.historyrender(
						
						sessionctx.profilectx,
						e);
			};
		
		form.get("profile_page.username").renderas = "p";
		form.get("profile_page.url").renderas = "p";
		
		var translation = pagedef.pagectx.translation("pt_BR");
		translation.put("follow", "Seguir");
		translation.put("home", "Meu perfil");
		translation.put("search", "Pesquisar");
		translation.put("unfollow", "Parar de seguir");
	}
	
	public static final DataObject follow(
			Context context,
			Session session,
			int op)
			throws Exception {
		String url, follower, following;
		FrontendContext sessionctx = session.sessionctx();
		var connection = session.connection();
		
		if (sessionctx.profilectx.user.url != null) {
			url = sessionctx.profilectx.user.url;
			
			follower = Compose.execute(
					sessionctx.homectx.user.id,
					"@",
					Shared.urlcompose(
							connection.scheme,
							"localhost",
							connection.port,
							connection.appname));
			
			following = Compose.execute(
					sessionctx.profilectx.user.id,
					"@",
					sessionctx.profilectx.user.url);
		} else {
			url = Shared.urlcompose(
					connection.scheme,
					"localhost",
					connection.port,
					connection.appname);
			
			follower = Long.toString(sessionctx.homectx.user.id);
			
			following = Long.toString(sessionctx.profilectx.user.id);
		}
		
		switch (op) {
		case FOLLOWER_ADD:
			sessionctx.profilectx.addFollower(follower);
			sessionctx.homectx.addFollowing(following);
			break;
		default:
			sessionctx.profilectx.removeFollower(follower);
			sessionctx.homectx.removeFollowing(following);
			break;
		}
		
		var profile = new HashMap<String, Object>();
		profile.put("user_profile.id", sessionctx.profilectx.user.id);
		profile.put("user_profile.url", sessionctx.profilectx.user.url);
		profile.put("user_profile.follower", sessionctx.homectx.user.id);
		profile.put("user_profile.appname", connection.appname);
		profile.put("user_profile.port", connection.port);
		profile.put("user_profile.followers", sessionctx.profilectx.getFollowers());
		profile.put("user_profile.following", sessionctx.homectx.getFollowing());
		
		var content = Send.execute(
				Compose.execute(url, "/gateway/"),
				"update_profile",
				sessionctx.logonctx.sessionid,
				profile);
		
		if (content == null)
			session.fail("profile.update.fail");
		
		sessionctx.homectx.data = Shared.downloadUserProfile(
				url,
				sessionctx.logonctx.sessionid,
				sessionctx.homectx.user.id);

		var datactx = context.getContextEntry(session.sessionid()).datactx;
		var type = session.type("user_profile");
		Conversion.execute(datactx, type, sessionctx.homectx.data);
		
		return session.output(sessionctx.homectx.data);
	}
	
	private final void followrender(Session session, PageElement element) {
		FrontendContext sessionctx = session.sessionctx();
		var userid = sessionctx.profilectx.user.toString();
		if (sessionctx.homectx.isFollowing(userid))
			element.cstyle = "invisible";
		else
			element.cstyle = visible;

		element.targets.put("click", "follow");
		element.outputupdate.instance("class").altname = "className";
		element.outputupdate.rendermode = PageElement.RENDER_ATTRIBUTES;
	}
	
	private final void unfollowrender(Session session, PageElement element) {
		FrontendContext sessionctx = session.sessionctx();
		var userid = sessionctx.profilectx.user.toString();
		if (sessionctx.homectx.isFollowing(userid))
			element.cstyle = visible;
		else
			element.cstyle = "invisible";

		element.targets.put("click", "unfollow");
		element.outputupdate.instance("class").altname = "className";
		element.outputupdate.rendermode = PageElement.RENDER_ATTRIBUTES;
	}
	
	public static final DataObject update(Session session) {
		FrontendContext sessionctx = session.sessionctx();
		
		var url = sessionctx.profilectx.user.url;
		if (url != null)
			url = Compose.execute("@", sessionctx.profilectx.user.url);

		var profilepage = session.output();
		profilepage.set("username", sessionctx.profilectx.user.username);
		profilepage.set("url", url);
		return profilepage;
	}
}

interface FollowerOp {
	
	public abstract void execute(
			String url,
			FrontendContext sessionctx,
			Map<String, Object> parameters,
			Set<String> followers,
			String follower) throws Exception;
	
}
