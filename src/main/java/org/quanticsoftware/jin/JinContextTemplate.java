package org.quanticsoftware.jin;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.quanticsoftware.automata.core.Context;
import org.quanticsoftware.automata.core.DataTypeExtensionHandler;
import org.quanticsoftware.automata.core.Fail;
import org.quanticsoftware.automata.servlet.ContextTemplate;
import org.quanticsoftware.automata.storage.MongoConnector;
import org.quanticsoftware.jin.backend.Backend;

public class JinContextTemplate {
	
	@SuppressWarnings("unchecked")
	public static final void config(Context context) {
		ContextTemplate.config(context);

		var dbname = System.getenv("JIN_DB");
		if (dbname == null)
			Fail.raise("undefined database name."
					+ " please set $JIN_DB system variable with db name.");

		var dbport = System.getenv("JIN_DB_PORT");
		if (dbport == null)
			Fail.raise("undefined database port."
					+ " please set $JIN_DB_PORT system variable with db port.");

		var ndbport = Integer.parseInt(dbport);
		context.setConnector(new MongoConnector("localhost", ndbport, dbname));
		
		context.setInstallDB((c,d,s)->{
			var typectx = context.typectx();
			
			var u = d.get(d.instance(typectx, "user", true));
			u.set("id", 1l);
			((Set<String>)u.get("accesses")).add("servers_config");
			Backend.register(u.data(), "admin", "admin");
			s.insert(u);

			var o = d.get(d.instance(typectx, "alias", true));
			o.set("id", "users_admin");
			o.set("nid", u);
			s.insert(o);
			
			o = d.get(d.instance(typectx, "number_object", true));
			o.set("type", "user");
			o.set("current", 1l);
			s.insert(o);
		});
		
		context.setGateway("/gateway/");
		
		/*
		 * types
		 */
		var typectx = context.typectx();
		
		var str_str_map_t = typectx.define(c->new HashMap<String, String>());
		var int_map_map_t = typectx.define(
				c->new HashMap<Integer, Map<String, Object>>());
		int_map_map_t.set(e->e.oconversion = (t,s)->{
			var maps = (Map<String, Map<String, Object>>)s;
			var mapt = new HashMap<Integer, Map<String, Object>>();
			
			for (var key : maps.keySet())
				mapt.put(Integer.parseInt(key), maps.get(key));
			
			return mapt;
		});
		
		var long_set_t = typectx.define(c->new HashSet<Long>());
		var str_list_t = typectx.define(c->new LinkedList<String>());
		var str_set_t = typectx.define(c->new HashSet<String>());
		
		DataTypeExtensionHandler list2set = (t,s)->{
			var source = (List<Object>)s;
			var target = (Set<Object>)t;
			target.clear();
			source.forEach(v->target.add(v));
			return target;
		};
		
		DataTypeExtensionHandler clist2set = (t,s)->{
			var source = (List<Object>)s;
			var target = (Set<Object>)new HashSet<Object>();
			source.forEach(v->target.add(v));
			return target;
		};
		
		// signup
		var signup_t = typectx.define("signup");
		signup_t.addst("username", "password", "confirm", "message");
		
		// logon
		var logon_t = typectx.define("logon");
		logon_t.addst("username", "secret", "message");
		
		// user, alias, register
		var user_t = typectx.define("user");
		user_t.addl("id");
		user_t.addst("username", "password");
		user_t.add("posts");
		user_t.add(long_set_t, "following", "followers");
		user_t.addl("last_post");
		user_t.add(str_set_t, "accesses");
		user_t.key("id");
		user_t.setAutoGenKey(true);
		
		var ext = user_t.extension("following");
		ext.dbinput = list2set;
		ext.oconversion = clist2set;
		
		ext = user_t.extension("followers");
		ext.dbinput = list2set;
		ext.oconversion = clist2set;
		
		ext = user_t.extension("accesses");
		ext.dbinput = list2set;
		ext.oconversion = clist2set;
		
		var alias_t = typectx.define("alias");
		alias_t.addst("id");
		alias_t.ref(user_t, "nid");
		alias_t.key("id");
		
		var register_collect_t = typectx.define("register_collect");
		register_collect_t.add(user_t, "user");
		register_collect_t.add(alias_t, "alias");
		register_collect_t.addst("sessionid");
		
		// front, posts, front_collect
		var front_page_t = typectx.define("front_page");
		front_page_t.addst("username", "status", "search", "message");
		front_page_t.add(str_str_map_t, "history");
		
		var posts_t = typectx.define("posts");
		posts_t.addst("id", "text");
		posts_t.key("id");

		var message_collect_t = typectx.define("message_collect");
		message_collect_t.ref(user_t, "user");
		message_collect_t.add(posts_t, "post");
		
		// search
		var search_page_t = typectx.define("search_page");
		search_page_t.addst("username", "message", "search", "chosen");
		search_page_t.add(str_str_map_t, "results");
		
		// profile
		var profile_page_t = typectx.define("profile_page");
		profile_page_t.addst("username", "url", "message", "search");
		profile_page_t.add(str_str_map_t, "history");
		
		var profile_collection_t = typectx.define("profile_collection");
		profile_collection_t.ref(user_t, "source_user", "target_user");
		
		// settings, servers
		var settings_page_t = typectx.define("settings_page");
		settings_page_t.addst("message", "url", "chosen");
		settings_page_t.add(str_list_t, "servers");
		
		var server_t = typectx.define("server");
		server_t.addl("id");
		server_t.addst("url");
		server_t.addi("type", "status");
		server_t.key("id");
		server_t.setAutoGenKey(true);
		
		// request
		var signin_t = typectx.define("signin");
		signin_t.add(user_t, "user");
		signin_t.addst("sessionid");
		
		var reg_req_t = typectx.define("reg_req");
		reg_req_t.addst("appname");
		reg_req_t.addi("port");
		
		typectx.define("req_st").addst("status");
		
		var search_req_t = typectx.define("search_req");
		search_req_t.addst("criteria");
		
		var search_list_t = typectx.define("search_list");
		search_list_t.add("results");
		
		var user_profile_t = typectx.define("user_profile");
		user_profile_t.addl("id", "last_post", "follower");
		user_profile_t.addst("type", "username", "appname", "url");
		user_profile_t.add(int_map_map_t, "posts");
		user_profile_t.add("followers", "following", "warnings", "accesses");
		user_profile_t.addi("port");
		
		user_profile_t.extension("following").oconversion = clist2set;
		user_profile_t.extension("followers").oconversion = clist2set;
		user_profile_t.extension("accesses").oconversion = clist2set;
	}

}
