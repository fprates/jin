package org.quanticsoftware.jin.frontend;

import javax.servlet.annotation.WebServlet;

import org.quanticsoftware.automata.core.Compose;
import org.quanticsoftware.automata.core.Context;
import org.quanticsoftware.automata.core.Fail;
import org.quanticsoftware.automata.servlet.AbstractApplicationServlet;
import org.quanticsoftware.automata.servlet.pages.Page;
import org.quanticsoftware.jin.JinContextTemplate;
import org.quanticsoftware.jin.home.HomePage;
import org.quanticsoftware.jin.logon.LogonPage;
import org.quanticsoftware.jin.profile.ProfilePage;
import org.quanticsoftware.jin.profileinfo.ProfileInfoPage;
import org.quanticsoftware.jin.search.SearchPage;
import org.quanticsoftware.jin.settings.SettingsPage;
import org.quanticsoftware.jin.signup.SignupPage;

@WebServlet(urlPatterns = {"/index.html",
		                   "/home/index.html", "/home",
		                   "/profile/index.html", "/profile",
		                   "/search/index.html", "/search",
		                   "/settings/index.html", "/settings",
		                   "/signup/index.html", "/signup"})
public class Servlet extends AbstractApplicationServlet {
	private static final long serialVersionUID = -806415126272383368L;

	@Override
	protected void init(Context context) {
		JinContextTemplate.config(context);
		
		context.setSendSessionIdSource(
				s->((FrontendContext)s.sessionctx()).logonctx.sessionid);
		
		context.setSessionctxFactory(()->new FrontendContext());
		
		context.setTargetExtractor((c,r)->{
			var t = (String)c.data.get("target");
			
			if (t == null) {
				var uri = r.getRequestURI();
				if (uri == null)
					return "init";
				
				var uritokens = uri.split("/");
				var last = uritokens.length - 1;
				if (uritokens[last].equals("index.html"))
					last--;
				
				var path = Compose.execute(uritokens[last], "/");
				t = context.getTargetByPath(path);
				
				if (t == null)
					return "init";
			}
			
			var target = context.getTarget(t);
			if (target == null)
				Fail.raise("no implementation for target '%s'.", t);

			var sessionctx = (FrontendContext)c.sessionctx;
			if (!target.isDisconnected() && !sessionctx.logonctx.connected)
				return "init";
			
			return t;
		});

		var typectx = context.typectx();
		var profile_page_t = typectx.get("profile_page");
		var front_page_t = typectx.get("front_page");
		var logon_t = typectx.get("logon");
		var signup_t = typectx.get("signup");
		var search_page_t = typectx.get("search_page");
		var server_t = typectx.get("server");
		var settings_page_t = typectx.get("settings_page");
		var signin_t = typectx.get("signin");
		var reqst_t = typectx.get("req_st");
		var register_collect_t = typectx.get("register_collect");
		var search_list_t = typectx.get("search_list");
		var message_collect_t = typectx.get("message_collect");
		
		/*
		 * logon facility
		 */
		var logonfac = context.facility("logon");
		var logonpage = context.page("page", p->{
			p.definition = new LogonPage();
			p.facility = logonfac;
			p.type = logon_t;
			p.rule = s->s.output();
		});
		
		/*
		 * home facility
		 */
		var homefac = context.facility("home");
		var homepage = context.page("page", p->{
			p.definition = new HomePage();
			p.facility = homefac;
			p.type = front_page_t;
			p.rule = s->HomePage.update(s);
			p.urlmapping = "home/";
		});
		
		context.page("profile_info", p->{
			p.definition = new ProfileInfoPage();
			p.facility = homefac;
			p.noupdate = true;
		});
		
		/*
		 * signup facility
		 */
		var signupfac = context.facility("signup");
		var signuppage = context.page("page", p->{
			p.definition = new SignupPage();
			p.facility = signupfac;
			p.type = signup_t;
			p.rule = s->s.output();
			p.urlmapping = "signup/";
		});
		
		/*
		 * search facility
		 */
		var searchfac = context.facility("search");
		var searchpage = context.page("page", p->{
			p.definition = new SearchPage();
			p.facility = searchfac;
			p.type = search_page_t;
			p.rule = s->SearchPage.update(s);
			p.urlmapping = "search/";
		});
		
		/*
		 * profile facility
		 */
		var profilefac = context.facility("profile");
		var profilepage = context.page("page", p->{
			p.definition = new ProfilePage();
			p.facility = profilefac;
			p.type = profile_page_t;
			p.rule = s->ProfilePage.update(s);
			p.urlmapping = "profile/";
		});
		
		/*
		 * settings facility
		 */
		var settingsfac = context.facility("settings");
		var settingspage = context.page("page", p->{
			p.definition = new SettingsPage();
			p.facility = settingsfac;
			p.type = settings_page_t;
			p.rule = s->SettingsPage.update(s);
			p.urlmapping = "settings/";
		});
		
		/*
		 * Targets
		 */
		
		context.target("connect").program(p->{
			p.send(signin_t, homefac, "sign_in");
			p.allinput(signin_t, homefac, s->LogonPage.signin(context, s));
			p.redirect(homepage);
			p.fail(logonpage, "logon.message");
			p.setDisconnected(true);
		});
		
		context.target("follow").program(p->{
			p.allinput(profile_page_t, profilefac, s->ProfilePage.follow(
					context,
					s,
					ProfilePage.FOLLOWER_ADD));
			p.call(profilepage.getFunction(Page.UPDATED_OUTPUT));
		});
		
		context.target("home_search").program(p->{
			p.send(search_list_t, homefac);
			p.allinput(search_list_t, homefac, s->SearchPage.mov2ctx(s));
			p.redirect(searchpage);
			p.fail(homepage, "front_page.status");
		});
		
		var target = context.target("init");
		target.pageinit(logonpage);
		target.setDisconnected(true);
		
		context.target("message_add").program(p->{
			p.allinput(front_page_t, homefac, s->{
				var frontpage = s.get("front_page");
				if (frontpage.getst("message") == null)
					s.fail("undefined.message");
				return frontpage;
			});
			p.send(message_collect_t, homefac);
			p.allinput(message_collect_t, homefac, s->HomePage.addMessage(s));
			p.call(homepage.getFunction(Page.UPDATED_OUTPUT));
			p.fail(homepage, "front_page.status");
		});
		
		context.target("profile.page_redirect").program(p->{
			p.setExceptionAsMessage(true);
			p.allinput(
					search_page_t,
					searchfac,
					s->SearchPage.getProfile(context, s));
			p.redirect(profilepage);
			p.fail(searchpage, "search_page.message");
		});
		
		context.target("profile_search").program(p->{
			p.send(search_list_t, profilefac);
			p.allinput(search_list_t, homefac, s->SearchPage.mov2ctx(s));
			p.redirect(searchpage);
			p.fail(profilepage, "profile_page.message");
		});
		
		context.target("search_search").program(p->{
			p.send(search_list_t, searchfac);
			p.allinput(search_list_t, searchfac, s->SearchPage.mov2ctx(s));
			p.call(searchpage.getFunction(Page.UPDATED_OUTPUT));
			p.fail(searchpage, "search_page.message");
		});
		
		context.target("server_add").program(p->{
			p.allinput(server_t, settingsfac, s->SettingsPage.addServer(s));
			p.call(settingspage.getFunction(Page.UPDATED_OUTPUT));
			p.fail(settingspage, "settings.message");
		});
		
//		context.target("server_remove").program(p->{
//			p.send(settingsfac);
//			p.call(settingspage.getFunction(Page.UPDATED_OUTPUT));
//		});
		
		context.target("sign_out").program(p->{
			p.send(reqst_t, homefac);
			p.allinput(reqst_t, homefac, s->{
				var sessionctx = (FrontendContext)s.sessionctx();
				sessionctx.logonctx.connected = false;
				sessionctx.logonctx.sessionid = null;
				
				sessionctx.homectx.user.id = 0;
				sessionctx.homectx.user.username = null;
				sessionctx.homectx.data = null;
				sessionctx.homectx.warnings.clear();
				
				return s.get("req_st");
			});
			p.redirect(logonpage);
		});
		
		context.target("signup_register").program(p->{
			p.setDisconnected(true);
			p.send(register_collect_t, signupfac);
			p.allinput(logon_t, signupfac, s->{
				var user = s.get("register_collect").data("user");
				var output = s.output();
				output.set("username", (String)user.get("username"));
				output.set("secret", (String)user.get("password"));
				return output;
			});
			p.send(signin_t, signupfac, "sign_in", "logon");
			p.allinput(signin_t, signupfac, s->LogonPage.signin(context, s));
			p.redirect(homepage);
			p.fail(signuppage, "signup.message");
		});
		
		context.target("unfollow").program(p->{
			p.allinput(profile_page_t, profilefac, s->ProfilePage.follow(
					context,
					s,
					ProfilePage.FOLLOWER_REMOVE));
			p.call(profilepage.getFunction(Page.UPDATED_OUTPUT));
		});
		
		for (var key : new String[] {
				"logon.page_redirect",
				"logon.page_init",
				"logon.page_controls_get",
				"signup.page_redirect",
				"signup.page_init",
				"signup.page_controls_get"
		})
			context.getTarget(key).setDisconnected(true);
	}

}
