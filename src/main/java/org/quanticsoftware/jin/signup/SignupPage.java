package org.quanticsoftware.jin.signup;

import org.quanticsoftware.automata.servlet.pages.DataFormTool;
import org.quanticsoftware.automata.servlet.pages.PageDefinitionContext;
import org.quanticsoftware.automata.servlet.pages.PageDefinition;
import org.quanticsoftware.jin.Shared;

public class SignupPage implements PageDefinition {

	@Override
	public void execute(PageDefinitionContext pagedef) {
		pagedef.elements.get("head").title = "signup";
		
		var container = Shared.execute(pagedef);
		
		container.button("logon").targets.put("click", "logon.page_redirect");
		
		var signup = DataFormTool.instance(container, pagedef.pagectx.type);
		
		var config = signup.config("message");
		config.einstance = (e,n)->e.p(n);
		config.nolabel = true;
		
		signup.config("password").einstance = (e,n)->e.password(n);
		signup.config("confirm").einstance = (e,n)->e.password(n);
		signup.build();
		
		container.submit("register").targets.put("click", "signup_register");
		
		var translation = pagedef.pagectx.translation("pt_BR");
		translation.put("logon", "Entrar");
		translation.put("register", "Criar conta");
		translation.put("signup.confirm", "Confirmar senhar");
		translation.put("signup.password", "Senha");
		translation.put("signup.username", "Usuário");
	}
}
