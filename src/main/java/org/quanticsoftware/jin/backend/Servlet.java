package org.quanticsoftware.jin.backend;

import javax.servlet.annotation.WebServlet;

import org.quanticsoftware.automata.core.Context;
import org.quanticsoftware.automata.core.Fail;
import org.quanticsoftware.automata.core.Send;
import org.quanticsoftware.automata.servlet.AbstractApplicationServlet;
import org.quanticsoftware.jin.JinContextTemplate;

@WebServlet(urlPatterns = {"/gateway/index.html", "/gateway"})
public class Servlet extends AbstractApplicationServlet {
	private static final long serialVersionUID = 5054477573766674910L;

	@Override
	protected void init(Context context) {
		JinContextTemplate.config(context);
		
		context.setSessionctxFactory(()->new BackendContext());
		
		context.setTargetExtractor((c,r)->{
			var t = (String)c.data.get("target");
			if (t == null)
				Fail.raise("undefined target.");
			
			var target = context.getTarget(t);
			if (target == null)
				Fail.raise("no implementation for target '%s'.", t);

			var sessionctx = (BackendContext)c.sessionctx;
			if (!target.isDisconnected() && !sessionctx.connected)
				Fail.raise("target '%s' can't be request disconnected.", t);
			
			return t;
		});

		var typectx = context.typectx();
		var user_t = typectx.get("user");
		var search_list_t = typectx.get("search_list");
		var user_profile_t = typectx.get("user_profile");
		var server_t = typectx.get("server");
		var req_st_t = typectx.get("req_st");
		var signin_t = typectx.get("signin");
		var register_collect_t = typectx.get("register_collect");
		var message_collect_t = typectx.get("message_collect");
		var profile_collect_t = typectx.get("profile_collection");
		var backend_f = context.facility("backend");
		
		var extarget = context.target("ex_target");
		extarget.program(p->{
			p.allinput(
					context.getOutputType(),
					backend_f,
					s->Send.returnError(s));
		});
		
		context.target("home_search").program(p->{
			p.allinput(
					search_list_t,
					backend_f,
					s->Backend.search(s, "front_page"));
			
			p.allinput(
					context.getOutputType(),
					backend_f,
					s->Send.returnRequest(s, "search_list"));
			
			p.fail(extarget);
		});

		context.target("message_add").program(p->{
			p.allinput(message_collect_t, backend_f, s->Backend.addMessage(s));
			p.collect(message_collect_t, backend_f, "user");
			p.collect(message_collect_t, backend_f, "post");
			
			p.allinput(
					context.getOutputType(),
					backend_f,
					s->Send.returnRequest(s, "message_collect"));
		});
		
		context.target("profile_search").program(p->{
			p.allinput(
					search_list_t,
					backend_f,
					s->Backend.search(s, "profile_page"));
			
			p.allinput(
					context.getOutputType(),
					backend_f,
					s->Send.returnRequest(s, "search_list"));
			
			p.fail(extarget);
		});
		
		context.target("server_register").program(p->{
			p.setDisconnected(true);
			p.allinput(
					server_t,
					backend_f,
					s->Backend.approveRequest(s));
			
			p.allinput(req_st_t, backend_f, s->{
				var output = s.output();
				output.set("status", "approved");
				return output;
			});
			
			p.allinput(
					context.getOutputType(),
					backend_f,
					s->Send.returnRequest(s, "req_st"));
		});
		
		context.target("search_get").program(p->{
			p.setDisconnected(true);
			p.allinput(
					search_list_t,
					backend_f,
					s->Backend.getResults(s));
			
			p.allinput(
					context.getOutputType(),
					backend_f,
					s->Send.returnRequest(s, "search_list"));
		});
		
		context.target("search_search").program(p->{
			p.allinput(
					search_list_t,
					backend_f,
					s->Backend.search(s, "search_page"));
			
			p.allinput(
					context.getOutputType(),
					backend_f,
					s->Send.returnRequest(s, "search_list"));
			
			p.fail(extarget);
		});
		
		context.target("sign_in").program(p->{
			p.setDisconnected(true);
			p.allinput(user_t, backend_f, s->Backend.getUser(s));
			p.allinput(signin_t, backend_f, s->Backend.signin(s));
			p.allinput(
					context.getOutputType(),
					backend_f,
					s->Send.returnRequest(s, "signin"));
			p.fail(extarget);
		});
		
		context.target("sign_out").program(p->{
			p.allinput(req_st_t, backend_f, s->{
				var sessionctx = (BackendContext)s.sessionctx();
				sessionctx.connected = false;
				sessionctx.user = null;
				
				var output = s.output();
				output.set("status", "success");
				return output;
			});
			p.allinput(
					context.getOutputType(),
					backend_f,
					s->Send.returnRequest(s, "req_st"));
		});
		
		context.target("signup_register").program(p->{
			p.setDisconnected(true);
			p.allinput(register_collect_t, backend_f, s->Backend.signup(s));
			p.collect(register_collect_t, backend_f, "user");
			p.collect(register_collect_t, backend_f, "alias");
			p.allinput(
					context.getOutputType(),
					backend_f,
					s->Send.returnRequest(s, "register_collect"));
			p.fail(extarget);
		});
		
		context.target("update_profile").program(p->{
			p.allinput(
					profile_collect_t,
					backend_f,
					s->Backend.updateProfile(s));
			p.collect(profile_collect_t, backend_f, "source_user");
			p.collect(profile_collect_t, backend_f, "target_user");
			p.allinput(
					context.getOutputType(),
					backend_f,
					s->Send.returnRequest(s, "user_profile"));
		});
		
		context.target("user_profile_get").program(p->{
			p.setDisconnected(true);
			
			p.allinput(
					user_profile_t,
					backend_f,
					s->Backend.getUserProfile(s));
			
			p.allinput(
					context.getOutputType(),
					backend_f,
					s->Send.returnRequest(s, "user_profile"));
			
			p.fail(extarget);
		});
	}

}
