package org.quanticsoftware.jin.backend;

import org.quanticsoftware.automata.core.DataObject;
import org.quanticsoftware.automata.core.SessionContext;

public class BackendContext implements SessionContext {

	public boolean connected;
	public DataObject user;
}
