package org.quanticsoftware.jin.backend;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;
import org.quanticsoftware.automata.core.Compose;
import org.quanticsoftware.automata.core.DataObject;
import org.quanticsoftware.automata.core.Send;
import org.quanticsoftware.automata.facilities.Session;
import org.quanticsoftware.jin.Shared;

public class Backend {
	public static final int INCOMING_REQUEST = 0;
	public static final int OUTGOING_REQUEST = 1;
	public static final int ACCEPTED = 0;
	
	public static final DataObject addMessage(Session session) {
		var frontpage = session.get("front_page");
		var output = session.output();
		
		BackendContext sessionctx = session.sessionctx();
		var post = frontpage.getst("message");
		if (post == null)
			return output;
		
		frontpage.set("message", null);
		var lastpost = sessionctx.user.getl("last_post") + 1;
		var userid = sessionctx.user.getl("id");
		var lastpostid = Shared.pidcompose(userid, lastpost);

		@SuppressWarnings("unchecked")
		var posts = (List<String>)sessionctx.user.get("posts");
		posts.add(lastpostid);
		
		sessionctx.user.set("last_post", lastpost);
		output.set("user", sessionctx.user);

		@SuppressWarnings("unchecked")
		var map = (Map<String, Object>)output.get("post");
		map.put("id", lastpostid);
		map.put("text", post);
		
		return output;
	}
	
	public static final DataObject approveRequest(Session session) {
		var regreq = session.get("reg_req");
		var connection = session.connection();
		
		var url = Shared.urlcompose(
				connection.scheme,
				connection.remoteurl,
				regreq.geti("port"),
				regreq.getst("appname"));
		
		var output = session.output();
		output.set("url", url);
		output.set("type", INCOMING_REQUEST);
		output.set("status", ACCEPTED);
		return output;
	}
	
	public static final DataObject getResults(Session session) {
		var searchreq = session.get("search_req");
		var alias = session.dbsession().get(
				session.type("alias"),
				Compose.execute("users_", searchreq.getst("criteria")));
		
		if (alias == null)
			session.fail("no.results.found");
		
		var list = new LinkedList<Map<String, Object>>();
		var output = session.output();
		var user = alias.data("nid");
		if (user != null)
			list.add(getUser(user));

		output.set("results", list);
		return output;
	}
	
	private static final JSONObject getResults(
			DataObject server,
			String sessionid,
			String criteria) throws Exception {
		
		var items = new HashMap<String, Object>();
		items.put("search_req.criteria", criteria);
		
		var output = Send.execute(
				server.getst("url"),
				"search_get",
				sessionid,
				items);
		return (output == null)? null : new JSONObject(output);
	}
	
	@SuppressWarnings("unchecked")
	public static final DataObject getUser(Session session) {
		var l = session.get("logon");
		var un = l.getst("username");
		var pw = l.getst("secret");
		if ((un == null) || (pw == null))
			session.fail("unspecified.credential");
			
		un = Compose.execute("users_", un.toLowerCase());
		var a = session.dbsession().get(session.type("alias"), un);
		if (a == null)
			session.fail("invalid.credential", un);
		
		var user = session.output();
		user.data((Map<String, Object>)a.get("nid"));
		
		if (!pw.equals(user.getst("password")))
			session.fail("invalid.credential");
		
		session.noupdate();
		
		return user;
	}
	
	private static final Map<String, Object> getUser(Map<String, Object> ouser) {
		var user = new HashMap<String, Object>();
		user.put("id", ouser.get("id"));
		user.put("name", Compose.execute("user: ", ouser.get("username")));
		return user;
	}
	
	public static final DataObject getUserProfile(Session session)
			throws Exception {
		var output = session.get("user_profile");
		var type = session.type(output.getst("type"));
		var dbsession = session.dbsession();
		var id = output.get("id");
		var object = dbsession.get(type, id);
		var posts_t = session.type("posts");
		var posts = new HashMap<Integer, Map<String, Object>>();
		var lastpost = object.getl("last_post");
		var last = lastpost > 10? lastpost - 10 : 1;
		var username = object.getst("username");
		var i = 0;
		
		for (long p = lastpost; p >= last; p--) {
			var pid = Shared.pidcompose((long)id, p);
			var post = new HashMap<String, Object>();
			post.put("author", username);
			post.put("text", dbsession.get(posts_t, pid).getst("text"));
			posts.put(i++, post);
		}
		
		var warnings = new LinkedList<Object[]>();
		var user_t = session.type("user");
		
		@SuppressWarnings("unchecked")
		var following = (Set<String>)object.get("following");
		for (var uid : following) {
			if (i == 10)
				break;
			
			var uidtokens = uid.split("@", 2);
			if (uidtokens.length == 2) {
				String content = null;
				JSONObject jobject = null;
				try {
					content = Shared.getUserProfile(
							uidtokens[1],
							"user",
							null,
							Long.parseLong(uidtokens[0]));
					
					jobject = new JSONObject(content);
				} catch (JSONException e) {
					warnings.add(new Object[] {
							"parsing.error",
							new Object[] {uidtokens[1]}
					});
					continue;
				} catch (Exception e) {
					warnings.add(new Object[] {
							"connect.error",
							new Object[] {uidtokens[1]}
					});
					continue;
				}
				
				for (var opost : jobject.getJSONArray("posts")) {
					if (i == 10)
						break;

					var post = ((JSONObject)opost).toMap();
					post.put("author", Compose.execute(
							post.get("author"),
							"@",
							uidtokens[1]));
					posts.put(i++, post);
				}
			} else {
				var tuser = dbsession.get(user_t, Long.parseLong(uid));
				@SuppressWarnings("unchecked")
				var tposts = (List<String>)tuser.get("posts");
				var uname = tuser.getst("username");
				
				for (var pid : tposts) {
					if (i == 10)
						break;
					
					var post = dbsession.get(posts_t, pid).data();
					post.put("author", uname);
					posts.put(i++, post);
				}
			}
		}
		
		output.set("last_post", lastpost);
		output.set("username", username);
		output.set("posts", posts);
		output.set("followers", object.get("followers"));
		output.set("following", following);
		output.set("warnings", session);
		output.set("accesses", object.get("accesses"));
		
		return output;
	}
	
	public static final void register(
			Map<String, Object> user,
			String username,
			String password) {
		user.put("username", username);
		user.put("password", password);
		user.put("posts", new LinkedList<String>());
		user.put("following", new HashSet<Long>());
		user.put("followers", new HashSet<Long>());
	}
	
	public static final DataObject search(
			Session session,
			String datapage) throws Exception {
		var search = session.get(datapage).getst("search").toLowerCase();
		var type = session.type("alias");
		var dbsession = session.dbsession();
		
		var items = new LinkedList<Map<String, Object>>();
		var searchlist = session.output();
		searchlist.set("results", items);
		
		var alias = dbsession.get(type, Compose.execute("users_", search));
		if (alias != null) {
			var ouser = getUser(alias.data("nid"));
			ouser.put("type", "user");
			items.add(ouser);
		}
		
		type = session.type("server");
		var servers = dbsession.getAll(type);
		if (servers != null)
			for (var server : servers) {
				if (server.geti("type") != OUTGOING_REQUEST)
					continue;
				
				JSONObject results = null;
				try {
					results = getResults(server, null, search);
				} catch (Exception e) {
					continue;
				}

				if (results == null)
					continue;
				
				var url = server.getst("url");
				
				for (var juser : results.getJSONArray("results")) {
					var user = (JSONObject)juser;
					var uname = user.getString("name");
					
					var item = user.toMap();
					item.put("type", "user");
					item.put("name", Compose.execute(uname, "@", url));
					items.add(item);
				}
			}
		
		if (items.size() == 0)
			session.fail("no.results.found");
		
		return searchlist;
	}
	
	public static final DataObject signin(Session session) {
		BackendContext sessionctx = session.sessionctx();
		
		sessionctx.user = session.get("user");
		sessionctx.connected = true;
		
		var output = session.output();
		output.set("user", sessionctx.user.data());
		output.set("sessionid", session.sessionid());
		return output;
	}
	
	public static final DataObject signup(Session session) {
		var signup = session.get("signup");
		var username = signup.getst("username").toLowerCase();
		var password = signup.getst("password");
		var key = Compose.execute("users_", username);
		
		var alias_t = session.type("alias");
		var alias = session.dbsession().get(alias_t, key);
		if (alias != null)
			session.fail("user.already.exist", username);
		
		if (!password.equals(signup.getst("confirm")))
			session.fail("password.mismatch");
		
		var register = session.output();

		register(register.data("user"), username, password);
		
		register.data("alias").put("id", key);
		register.set("sessionid", session.sessionid());
		
		return register;
	}
	
	public static final DataObject updateProfile(Session session) {
		var profile = session.get("user_profile");
		
		var user_t = session.type("user");
		var dbsession = session.dbsession();
		var follower = dbsession.get(user_t, profile.getl("follower"));
		follower.set("following", profile.get("following"));
		
		var collection = session.output();
		var url = profile.getst("url");
		if (url == null) {
			var followed = dbsession.get(user_t, profile.getl("id"));
			followed.set("followers", profile.get("followers"));
			collection.set("target_user", followed);
		}

		collection.set("source_user", follower);
		
		return collection;
	}
}
