package org.quanticsoftware.jin.home;

import java.util.HashMap;
import java.util.Map;

import org.quanticsoftware.automata.core.DataObject;
import org.quanticsoftware.automata.facilities.Session;
import org.quanticsoftware.automata.servlet.pages.DataFormTool;
import org.quanticsoftware.automata.servlet.pages.PageDefinitionContext;
import org.quanticsoftware.automata.servlet.pages.PageElement;
import org.quanticsoftware.automata.servlet.pages.PageDefinition;
import org.quanticsoftware.jin.Shared;
import org.quanticsoftware.jin.frontend.FrontendContext;

public class HomePage implements PageDefinition {
	
	public static final DataObject addMessage(Session session) {
		var frontpage = session.get("front_page");
		var text = frontpage.getst("message");
		var sessionctx = (FrontendContext)session.sessionctx();
		
		frontpage.set("message", null);
		
		var messagecollect = session.get("message_collect");
		var user = (DataObject)messagecollect.get("user");
		sessionctx.homectx.user.id = user.getl("id");
		sessionctx.homectx.user.username = user.getst("username");
		
		// shift messages 1 place ahead
		var oposts = sessionctx.homectx.data.get("posts");
		@SuppressWarnings("unchecked")
		var posts = (Map<Integer, Map<String, Object>>)oposts;
		for (int i = posts.size() - 1; i >= 0; i--)
			posts.put(i + 1, posts.get(i));
		
		// put new message at the top of the posts
		var post = new HashMap<String, Object>();
		post.put("author", sessionctx.homectx.user.username);
		post.put("text", text);
		posts.put(0, post);
		
		return messagecollect;
	}

	@Override
	public final void execute(PageDefinitionContext pagedef) {
		pagedef.elements.get("head").title = "home";
		
		var container = Shared.execute(pagedef);
		
		container.button("signout").targets.put("click", "sign_out");
		
		var button = container.button("settings");
		button.targets.put("click", "settings.page_redirect");
		
		var form = DataFormTool.instance(container, pagedef.pagectx.type);
		
		form.config("search").nolabel = true;
		
		var config = form.config("username");
		config.nolabel = true;
		config.einstance = (e,n)->e.custom("h1", n);
		
		config = form.config("status");
		config.nolabel = true;
		config.einstance = (e,n)->e.p(n);
		
		form.config("message").einstance = (e,n)->e.textarea(n);
		
		config = form.config("history");
		config.nolabel = true;
		config.einstance = (e,n)->e.div(n);
		
		form.build();
		
		var info = form.get("username_group").a("info");
		info.value = "info.available";
		info.targets.put("click", "home.profile_info_redirect");
		info.renderer = (s,e)->{
			FrontendContext sessionctx = s.sessionctx();
			if (sessionctx.homectx.warnings.size() > 0)
				e.cstyle = "link-primary";
			else
				e.cstyle = "invisible";
			
			e.outputupdate.instance("class").altname = "className";
			e.outputupdate.rendermode = PageElement.RENDER_ATTRIBUTES;
		};
		
		form.get("search_group").
			button("search").
			targets.put("click", "home_search");
		
		form.get("front_page.history").renderer = (s,e)->{
			FrontendContext sessionctx = (FrontendContext)s.sessionctx();
			Shared.historyrender(sessionctx.homectx, e);
		};
		
		form.get("message_group").
			button("message_add").
			targets.put("click", "message_add");
		
		form.get("front_page.history").cstyle = "list-group";
		
		form.get("front_page.username").renderer = (s,e)->e.renderas = "p";
		
		var translation = pagedef.pagectx.translation("pt_BR");
		translation.put("front_page.message", "Nova mensagem");
		translation.put("info.available", "Informação adicionais");
		translation.put("message_add", "Postar");
		translation.put("search", "Pesquisar");
		translation.put("settings", "Configurar");
		translation.put("signout", "Sair");
	}
	
	public static final DataObject logout(Session session) {
		FrontendContext sessionctx = session.sessionctx();
		sessionctx.logonctx.connected = false;
		return session.get("front_page");
	}
	
	public static final DataObject update(Session session) throws Exception {
		FrontendContext sessionctx = session.sessionctx();
		
		var frontpage = session.output();
		
		frontpage.set("username", sessionctx.homectx.user.username);
		
		return frontpage;
	}
}
