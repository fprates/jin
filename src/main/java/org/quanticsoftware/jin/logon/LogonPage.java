package org.quanticsoftware.jin.logon;

import org.quanticsoftware.automata.core.Compose;
import org.quanticsoftware.automata.core.Context;
import org.quanticsoftware.automata.core.Conversion;
import org.quanticsoftware.automata.core.DataObject;
import org.quanticsoftware.automata.facilities.Session;
import org.quanticsoftware.automata.servlet.pages.DataFormTool;
import org.quanticsoftware.automata.servlet.pages.PageDefinitionContext;
import org.quanticsoftware.automata.servlet.pages.PageDefinition;
import org.quanticsoftware.jin.Shared;
import org.quanticsoftware.jin.frontend.FrontendContext;

public class LogonPage implements PageDefinition {
	
	@Override
	public final void execute(PageDefinitionContext pagedef) {
		var head = pagedef.elements.get("head");
		head.title = "logon";
		
		var link = head.link();
		link.parameters.put("href", Compose.execute(
				pagedef.appname,
				"/signin.css"));
		
		link.parameters.put("rel", "stylesheet");
		
		var container = Shared.execute(pagedef);
		var body = pagedef.elements.get("body");
		body.get("main").cstyle = "form-signin";
		body.cstyle = "text-center";
		
		var signinplease = container.element("h1", "signin_please");
		signinplease.renderas = "p";
		signinplease.cstyle = "h3 mb-3 fw-normal";
		signinplease.value = "signin_please";
		signinplease.setTranslateable(true);
		
		var logon = DataFormTool.instance(container, pagedef.pagectx.type);
		logon.setPlaceholder(true);
		
		var config = logon.config("message");
		config.einstance = (e,n)->e.p(n);
		config.nolabel = true;
		
		logon.config("secret").einstance = (e,n)->e.password(n);
		logon.build();
		
		logon.get("username_group").cstyle = "form-floating";
		
		logon.get("secret_group").cstyle = "form-floating";
		
		var button = container.submit("connect");
		button.targets.put("click", "connect");
		button.cstyle = "w-100 btn btn-lg btn-primary";
		
		container.a("signup").targets.put("click", "signup.page_redirect");
		
		var translation = pagedef.pagectx.translation("pt_BR");
		translation.put("connect", "Entrar");
		translation.put("logon.secret", "Senha");
		translation.put("logon.username", "Usuário");
		translation.put("signin_please", "Bem vindo");
		translation.put("signup", "Nova conta");
	}
	
	public static final DataObject signin(Context context, Session session)
			throws Exception {
		FrontendContext sessionctx = session.sessionctx();
		
		var input = session.get("signin");
		sessionctx.logonctx.connected = true;
		sessionctx.logonctx.sessionid = input.getst("sessionid");
		
		var connection = session.connection();
		
		var url = Shared.urlcompose(
				connection.scheme,
				"localhost",
				connection.port,
				connection.appname);
		
		var user = input.data("user");
		sessionctx.homectx.user.id = (long)user.get("id");
		sessionctx.homectx.user.username = (String)user.get("username");
		sessionctx.homectx.data = Shared.downloadUserProfile(
				url,
				sessionctx.logonctx.sessionid,
				sessionctx.homectx.user.id);

		var datactx = context.getContextEntry(session.sessionid()).datactx;
		var type = session.type("user_profile");
		Conversion.execute(datactx, type, sessionctx.homectx.data);
		
		return input;
	}
}
