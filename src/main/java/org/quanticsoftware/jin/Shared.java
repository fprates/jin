package org.quanticsoftware.jin;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.quanticsoftware.automata.core.Compose;
import org.quanticsoftware.automata.core.Send;
import org.quanticsoftware.automata.servlet.pages.PageDefinitionContext;
import org.quanticsoftware.automata.servlet.pages.PageElement;
import org.quanticsoftware.jin.profile.ProfileContext;

public class Shared  {
	private static final Map<String, UserSet> usets;
	
	static {
		usets = new HashMap<>();
		usets.put("followers", (u,p)->p.addFollower(u));
		usets.put("following", (u,p)->p.addFollowing(u));
	}
	
	public static final Map<String, Object> downloadUserProfile(
			String url,
			String sessionid,
			long userid) throws Exception {
		var content = Shared.getUserProfile(
				Compose.execute(url, "/gateway/"),
				sessionid,
				"user",
				userid);
		
		return new JSONObject(content).getJSONObject("data").toMap();
	}
	
	public static final PageElement execute(PageDefinitionContext pagedef) {
		pagedef.pagectx.page.setDocType("<!doctype html>");
		
		var head = pagedef.elements.get("head");
		head.meta().parameters.put("charset", "utf-8");
		
		var meta = head.meta();
		meta.parameters.put("name", "viewport");
		meta.parameters.put("content", "width=device-width, initial-scale=1, shrink-to-fit=no");
		
		var link = head.link();
		link.parameters.put("href", "https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css");
		link.parameters.put("rel", "stylesheet");
		link.parameters.put("integrity", "sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1");
		link.parameters.put("crossorigin", "anonymous");
		
		var container = pagedef.elements.get("body").
				form("main").
				div("container");
		
		container.cstyle = "container";

		for (var file : new String[] {"/jquery-3.5.1.js", "/handlers.js"})
			head.script("text/javascript").src = Compose.execute(
					pagedef.appname,
					file);
		
		head.script("text/javascript").value = Compose.execute(
				"qs_register_all('",
				pagedef.pagectx.facility.name(),
				"', '",
				pagedef.pagectx.page.name(),
				"');");

		pagedef.pagectx.template.css.put("a", "link-primary");
		pagedef.pagectx.template.css.put("div", "mb-3");
		pagedef.pagectx.template.css.put("input-text", "form-control");
		pagedef.pagectx.template.css.put("input-button", "btn btn-primary");
		pagedef.pagectx.template.css.put("label", "form-label");
		pagedef.pagectx.template.css.put("li", "list-group-item");
		pagedef.pagectx.template.css.put("p", "form-text");
		pagedef.pagectx.template.css.put("textarea", "form-control");
		pagedef.pagectx.template.css.put("ul", "list-group");
		
		return container;
	}

	public static final String getUserProfile(
			String url,
			String sessionid,
			String type,
			long id) throws Exception {
		var items = new HashMap<String, Object>();
		items.put("user_profile.id", id);
		items.put("user_profile.type", type);
		
		return Send.execute(url, "user_profile_get", sessionid, items);
	}
	
	public static final void historyrender(
			ProfileContext homectx,
			PageElement element) {
		element.clear();

		@SuppressWarnings("unchecked")
		var posts = (Map<Integer, Map<String, Object>>)homectx.data.get("posts");
		for (int i = 0; i < posts.size(); i++) {
			var hitem = posts.get(i);
			
			var id = Compose.execute("author_", i);
			var author = element.div(id);
			author.cstyle = "list-group-item list-group-item-action flex-column align-items-start";
			author.setTranslateable(false);
			
			var aitem = author.element("h6", id);
			aitem.value = (String)hitem.get("author");
			aitem.renderas = "p";
			aitem.setTranslateable(false);
			
			var text = author.p(Compose.execute("text_", i));
			text.value = (String)hitem.get("text");
			text.setTranslateable(false);
		}
		
	}
	
	public static final String pidcompose(long uid, long pid) {
		return String.format("%08d-%08d", uid, pid);
	}
	
	public static final String urlcompose(
			String scheme,
			String url,
			int port,
			String appname) {
		return Compose.execute(scheme, "://", url, ":", port, appname);
	}
}

interface UserSet {

	public abstract void execute(String user, ProfileContext profilectx);
	
}
