package org.quanticsoftware.jin.settings;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;
import org.quanticsoftware.automata.core.Compose;
import org.quanticsoftware.automata.core.DataObject;
import org.quanticsoftware.automata.core.Send;
import org.quanticsoftware.automata.facilities.Session;
import org.quanticsoftware.automata.servlet.pages.DataFormTool;
import org.quanticsoftware.automata.servlet.pages.PageDefinition;
import org.quanticsoftware.automata.servlet.pages.PageDefinitionContext;
import org.quanticsoftware.automata.servlet.pages.PageElement;
import org.quanticsoftware.jin.Shared;
import org.quanticsoftware.jin.backend.Backend;
import org.quanticsoftware.jin.frontend.FrontendContext;

public class SettingsPage implements PageDefinition {
	private static final String[] reqtypes, status;
	
	static {
		reqtypes = new String[] {
				"incoming.request",
				"outgoing.request"
		};
		
		status = new String[] {
				"accepted"
		};
	}
	
	public static final DataObject addServer(Session session) throws Exception {
		var settings = session.get("settings_page");
		var url = settings.getst("url");
		if (url == null)
			session.fail("undefined.url");
		
		var routput = requestRegistry(session, url);
		if (routput == null)
			session.fail("connection.error");
		
		try {
			var joutput = new JSONObject(routput);
			var data = (JSONObject)joutput.get("data");
			if (!data.getString("status").equals("approved"))
				session.fail("rejected.connection");
		} catch(JSONException e) {
			session.fail("invalid.answer");
		}

		FrontendContext sessionctx = session.sessionctx();
		sessionctx.settingsctx.lastserver++;
		var sid = Long.toString(sessionctx.settingsctx.lastserver);
		
		var server = new ServerContext(sessionctx.settingsctx.lastserver);
		server.url = url;
		server.type = reqtypes[Backend.OUTGOING_REQUEST];
		server.status = status[Backend.ACCEPTED];
		sessionctx.settingsctx.servers.put(sid, server);
		
		settings.set("url", null);
		
		var output = session.output();
		output.set("url", url);
		output.set("type", Backend.OUTGOING_REQUEST);
		output.set("status", Backend.ACCEPTED);
		return output;
	}
	
	@Override
	public final void execute(PageDefinitionContext pagedef) {
		pagedef.elements.get("head").title = "settings";
		
		var container = Shared.execute(pagedef);
		
		container.button("home").targets.put("click", "home.page_redirect");
		
		var form = DataFormTool.instance(container, pagedef.pagectx.type);
		
		var config = form.config("message");
		config.einstance = (e,n)->e.p(n);
		config.nolabel = true;
		
		config = form.config("chosen");
		config.nolabel = true;
		config.einstance = (e,n)->e.parameter(n);
		
		form.config("servers").einstance = (e,n)->e.div(n);
		
		form.build();
		
		form.get("url_group").renderer = (s,e)->{
			FrontendContext appctx = (FrontendContext)s.sessionctx();
			var show = appctx.homectx.checkAuthority("servers_config");
			e.cstyle = show? "form-group" : "invisible";
			e.outputupdate.rendermode = PageElement.RENDER_NO_RENDER;
		};
		
		form.get("servers_group").renderer = (s,e)->{
			FrontendContext appctx = (FrontendContext)s.sessionctx();
			var show = appctx.homectx.checkAuthority("servers_config");
			e.cstyle = show? "form-group" : "invisible";
		};
		
		form.get("settings_page.servers").renderer = (s,e)->serverlist(s,e);
		
		form.get("url_group").button("add").targets.put("click", "server_add");
		
		var translation = pagedef.pagectx.translation("pt_BR");
		translation.put("accepted", "Aceito");
		translation.put("add", "Adicionar");
		translation.put("home", "Meu perfil");
		translation.put("outgoing.request", "Seguindo");
		translation.put("settings_page.servers", "Servidores");
		translation.put("settings_page.url", "Novo servidor");
	}
	
	private final void serverlist(Session session, PageElement element) {
		FrontendContext sessionctx = session.sessionctx();
		element.clear();
		
		var eservers = element.ul("servers");
		for (var sid : sessionctx.settingsctx.servers.keySet()) {
			var server = sessionctx.settingsctx.servers.get(sid);

			var item = eservers.li(sid);
			item.cstyle = "list-group-item list-group-item-success";

//			var button = item.button(Compose.execute("remove_", sid));
//			button.value = "remove";
//			button.targets.put("click", "server_remove");
//			button.cparameters.put("sourcep", "settings_page.chosen");
//			button.cparameters.put("sourcev", sid);
			
			var h = item.element("h5", Compose.execute("url_", sid));
			h.value = server.url;
			h.renderas = "p";
			
			h = item.element("h6", Compose.execute("type_", sid));
			h.value = server.type;
			h.renderas = "p";
			h.setTranslateable(true);
			
			h = item.element("h7", Compose.execute("st_", sid));
			h.value = server.status;
			h.renderas = "p";
			h.setTranslateable(true);
		}
	}
	
	public static final DataObject removeServer(Session session) {
		var settings = session.get("settings_page");
		var sid = settings.getst("chosen");
		FrontendContext sessionctx = session.sessionctx();
		var serverctx = sessionctx.settingsctx.servers.get(sid);
		
		var type = session.type("server");
		session.dbsession().remove(type, serverctx.id);

		sessionctx.settingsctx.servers.remove(sid);
		
		return settings;
	}
	
	private static final String requestRegistry(
			Session session,
			String spec) throws Exception {
		var connection = session.connection();
		
		var items = new HashMap<String, Object>();
		items.put("reg_req.appname", connection.appname + "/");
		items.put("reg_req.port", connection.port);
		
		FrontendContext sessionctx = session.sessionctx();
		return Send.execute(
				Compose.execute(spec, "/gateway/"),
				"server_register",
				sessionctx.logonctx.sessionid,
				items);
	}
	
	public static final DataObject update(Session session) {
		var oservers = session.dbsession().getAll(session.type("server"));
		FrontendContext sessionctx = session.sessionctx();
		
		sessionctx.settingsctx.servers.clear();
		for (var oserver : oservers) {
			var id = oserver.getl("id");
			if (id > sessionctx.settingsctx.lastserver)
				sessionctx.settingsctx.lastserver = id;
			
			var server = new ServerContext(id);
			server.url = oserver.getst("url");
			server.type = reqtypes[oserver.geti("type")];
			server.status = status[oserver.geti("status")];
			sessionctx.settingsctx.servers.put(Long.toString(id), server);
		}
		
		var output = session.get("settings_page");
		return (output == null)? session.output() : output;
	}
}
