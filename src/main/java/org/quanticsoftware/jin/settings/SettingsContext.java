package org.quanticsoftware.jin.settings;

import java.util.HashMap;
import java.util.Map;

public class SettingsContext {
	public long lastserver;
	public Map<String, ServerContext> servers;
	
	public SettingsContext() {
		servers = new HashMap<>();
	}
}
